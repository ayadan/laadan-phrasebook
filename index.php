
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Láadan Quick Search Dictionary - By Rachel Singh</title>
    <script src="content/jquery-2.2.4/jquery-2.2.4.min.js"></script>
        
    <link href="content/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="content/bootstrap-3.3.6-dist/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- <link href="content/jumbotron-narrow.css" rel="stylesheet"> -->
    <!-- <script src="content/bootstrap-3.3.6-dist/js/ie-emulation-modes-warning.js"></script> -->

    <style type="text/css">
        .breakdown { background: #c9e1ff; }
        .unofficial { background: #ffc9db; }

        .breakdown, .unofficial { font-size: 0.9em; border-radius: 5px; text-align: left; padding: 5px; font-weight: bold; }
    </style>
  </head>

  <body>

    <? include_once( "backend.php" ); ?>
    
    <script type="text/javascript">
        $( function() {
            
            $( "#filterButton" ).click( function() {
                    BeginFilter();
            } ); // click

            $( document ).keypress( function( e ) {
                if ( e.keyCode == 13 )
                {
                    BeginFilter();
                }
            } );

            function BeginFilter()
            {
                $( "#filterButton" ).html( "Searching..." );
                $( "#filterButton" ).removeClass( "btn-success" );
                $( "#filterButton" ).addClass( "btn-warning" );
                
                setTimeout( function() {
                    Filter();
                }, 1000 );
            }

            function EndFilter()
            {
                $( "#filterButton" ).html( "Filter" );
                $( "#filterButton" ).addClass( "btn-success" );
                $( "#filterButton" ).removeClass( "btn-warning" );
            }

            function Sanitize( str )
            {
                str = str.toLowerCase();
                str = str.replace( "á", "a" );
                str = str.replace( "é", "e" );
                str = str.replace( "í", "i" );
                str = str.replace( "ó", "o" );
                str = str.replace( "ú", "u" );
                return str;
            }

            function Hide( element )
            {
                $( element ).css( "display", "none" );
                //$( element ).fadeOut( "fast" );
            }

            function Show( element )
            {
                $( element ).css( "display", "table-row" );
                //$( element ).fadeIn( "fast" );
            }

            function ShowAll()
            {
                var classification = $('#find-what').find(":selected").text();
                if ( classification == "all classifications" ) { classification = ""; }
                
                $.each( $( ".dictionary-entry" ), function( key, element ) {
                    var entryClass          = Sanitize( $( element ).attr( "classification" ) );
                    if ( classification == "" || StringContains( entryClass, classification )  )
                    {
                        Show( element );
                    }
                    else
                    {
                        Hide( element );
                    }
                } );

                EndFilter();
            }

            function StringContains( fullString, subString )
            {
                return ( fullString.indexOf( subString ) >= 0 );
            }
        
            function Filter()
            {
                var searchTerm = Sanitize( $( "#searchTerm" ).val() );
                
                if ( searchTerm == "" )
                {
                    ShowAll();
                    return;
                }
                
                var checkWhat = "all";
                if      ( $( "#search-laadan" ).prop( "checked" ) == true )         { checkWhat = "laadan"; }
                else if ( $( "#search-english" ).prop( "checked" ) == true )        { checkWhat = "english"; }

                var classification = $('#find-what').find(":selected").text();
                if ( classification == "all classifications" ) { classification = ""; }

                var itemCount = 0;
                var shown = 0;
                var hidden = 0;
                
                $.each( $( ".dictionary-entry" ), function( key, element ) {
                    var entryLaadan         = Sanitize( $( element ).attr( "laadan" ) );
                    var entryEnglish        = Sanitize( $( element ).attr( "english" ) );
                    var entryClass          = Sanitize( $( element ).attr( "classification" ) );
                    var entryDesc          = Sanitize( $( element ).attr( "description" ) );
                    itemCount++;

                    if ( checkWhat == "all" )
                    {
                        if (
                            ( StringContains( entryLaadan, searchTerm ) ||
                            StringContains( entryEnglish, searchTerm ) ||
                            StringContains( entryDesc, searchTerm )
                            ) &&
                            ( classification == "" || StringContains( entryClass, classification ) )
                            ) {
                            Show( element );
                            shown++;
                        }
                        else
                        {
                            Hide( element );
                            hidden++;
                        }
                    }
                    else if ( checkWhat == "laadan" )
                    {
                        if (
                            StringContains( entryLaadan, searchTerm ) &&
                            ( classification == "" || StringContains( entryClass, classification ) )
                            ) {
                            Show( element );
                            shown++;
                        }
                        else {
                            Hide( element );
                            hidden++;
                        }
                    }
                    else if ( checkWhat == "english" )
                    {
                        if ( 
                            StringContains( entryEnglish, searchTerm ) &&
                            ( classification == "" || StringContains( entryClass, classification ) )
                            ) { 
                            Show( element );
                            shown++;
                        }
                        else {
                            Hide( element );
                            hidden++;
                        }
                    }
                } ); // each

                EndFilter();
            }
            
        } ); // ready
        
        
    </script>

    <div class="container">
      <div class="header clearfix">
        <nav>
          
        </nav>
        <h3 class="text-muted">Láadan Dictionary</h3>
      </div>

      <div class="jumbotron">
        <div class="row">
            <div class="col-md-9">
                <p><input type="text" name="searchTerm" id="searchTerm" class="form-control" placeholder="Search term(s)"></p>
            </div>
            <div class="col-md-3">
                <select id="find-what" class="form-control">
                    <option>all classifications</option>
                    <option>noun</option>
                    <option>core word</option>
                    <option>transitive verb</option>
                    <option>intransitive verb</option>
                    <option>speech act morpheme</option>
                    <option>evidence morpheme</option>
                    <option>affix</option>
                    <option>marker</option>
                    <option>first declension</option>
                    <option>second declension</option>
                </select>
            </div>
        </div>
        <hr>
        <div class="row refine-search">
            <div class="col-md-4"> <input type="radio" value="" name="search-what" id="search-all" checked="checked">
            <label for="search-all">Search all</label><br><sub>(English, Láadan, and Description)</sub> </div>
            <div class="col-md-4"> <input type="radio" value="" name="search-what" id="search-laadan">
            <label for="search-laadan">Search only Láadan</label> </div>
            <div class="col-md-4"> <input type="radio" value="" name="search-what" id="search-english">
            <label for="search-english">Search only English</label> </div>
        </div>
        <hr>
        <p><a class="btn btn-lg btn-success pull-right btn-block" href="#" role="button" id="filterButton">Filter</a></p>
        <br>
      </div>

      <div class="row">  
          
          <table class="table table-striped">
              <thead>
                  <tr>
                      <th style="width:5%;">#</th>
                      <th style="width:20%;">Láadan</th>
                      <th style="width:20%;">English</th>
                      <th style="width:40%;">Description</th>
                      <th style="width:15%;">Classification</th>
                  </tr>
              </thead>
              <tbody>
                  <? foreach( $json as $index=>$entry ) { ?>
                      <tr class="dictionary-entry" english="<?=$entry["english"] ?>" laadan="<?=$entry["láadan"] ?>" description="<?=htmlspecialchars($entry["description"]) ?>" classification="<?=$entry["classification"] ?>">
                          <td><?=($index+1)?></td>
                          <td><?=$entry["láadan"] ?></td>
                          <td>
                              <p><?=$entry["english"] ?></p>
                          </td>
                          <td>
                              <p><?=$entry["description"] ?></p>
                              <? if ( $entry["notes"] != "" ) { ?>
                              <p><?=$entry["notes"] ?></p>
                              <? } ?>
                              <? if ( $entry["word breakdown"] != "" ) { ?>
                              <p class="breakdown"><?=$entry["word breakdown"] ?></p>
                              <? } ?>
                              <? if ( $entry["unofficial"] != "" ) { ?>
                              <p class="unofficial"><?=$entry["unofficial"] ?></p>
                              <? } ?>
                          </td>
                          <td><?=$entry["classification"] ?></td>
                      </tr>
                  <? } ?>
              </tbody>
          </table> 
      
      </div>
      
      <hr>

      <footer class="footer">
        <p>Programmed by Rachel Wil Sha Singh (Rachel@Moosader.com)</p>
        <p>Dictionary from <a href="https://laadanlanguage.wordpress.com/">laadanlanguage.wordpress.com</a></p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
